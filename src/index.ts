import readline from 'readline';
import { ft } from './file_tree/index';
import { FileColors } from './file_tree/MyFile';
import { FolderColors, MyFolder } from './file_tree/MyFolder';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let cwd = ft.rootFolder;

rl.on('line', (input: string) => {
    if(input.indexOf('ls') === 0) {
        ft.printFiles(cwd);
    }

    if(input.indexOf('cd') === 0) {
        const fileName = input.substr(3);
        if(fileName === '..') {
            cwd = ft.getFile(cwd.parentId);
        } else {
            const file = ft.findByName(cwd, fileName);
    
            if(file && file.value instanceof MyFolder) {
                cwd = file.value;
            }
        }
    }

    if(input.indexOf('rm') === 0) {
        const fileName = input.substr(3);
        const file = ft.findByName(cwd, fileName);
        if(file) {
            cwd.deleteFile(file.value.id)        
        }
    }

    if(input.indexOf('mkdir') === 0) {
        const dirName = input.substr(6);

        ft.addDir(dirName, Math.floor(Math.random() * 100000), cwd.id, FolderColors.Blue);
    }

    if(input.indexOf('touch') === 0) {
        const fileName = input.substr(6);

        ft.addFile(fileName, Math.floor(Math.random() * 100000), cwd.id, FileColors.Blue);
    }
});