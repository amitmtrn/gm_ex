/**
 * File:
 *   id: number
 *   name: string
 *   color: Enum(white, blue)
 * 
 * Directory:
 *   id: number
 *   name: string (32 chars)
 *   color: Enum(red, green, blue)
 * 
 *  write a program that have the api:
 * 
 *  addDir(name, id, color, parentId)
 *  addFile(name, id, color, parentId)
 *  move(id, targetParentId)
 *  delete(id)
 *  printAll(id?)
 * 
 *  can't use collection of ES include [] {}
 *  input is valid and unique (example ids)
 *  everything is in memory
 *  can use stdout to print
 *
 */

import { ListItem } from "./ListItem";
import { FileColors, MyFile } from "./MyFile";
import { FolderColors, MyFolder } from "./MyFolder";
import { OriginFile } from "./OriginFile";

const rootFolder = new MyFolder(0, 'root', -1, FolderColors.Blue);

function findFile(fileId: number, root: MyFolder = rootFolder): OriginFile | null {   
    if(root.id === fileId) {
        return root;
    }

    if(!root.files) {
        return null;
    }
    
    let file: ListItem | null = root.files;

    while(file) {
        if(file.value.id === fileId) {
            return file.value;
        }

        if(file.value instanceof MyFolder) {
            const nextRoot: OriginFile | null = findFile(fileId, file.value);
            
            if(nextRoot !== null) {
                return nextRoot;
            }
        }

        file = file.next;
    }

    return null;
}

function printAll(id: number = 0){
    const dir: MyFolder = findFile(id) as MyFolder;
    let file = dir.files;

    while(file) {
        console.log(file.value.name);

        if(file.value instanceof MyFolder) {
            printAll(file.value.id);
        }

        file = file.next;
    }

}

export const ft = {
    rootFolder,
    findByName(folder: MyFolder, fileName: string) {
        return folder.findByName(fileName);
    },
    getFile(fileId: number) {
        return findFile(fileId) as MyFolder;
    },
    addDir(name: string, id: number, parentId: number, color: FolderColors) {
        const dir: MyFolder = findFile(parentId) as MyFolder;
        dir.addFile(new MyFolder(id, name, parentId, color));
    },
    addFile(name: string, id: number, parentId: number, color: FileColors) {
        const dir: MyFolder = findFile(parentId) as MyFolder;
        dir.addFile(new MyFile(id, name, parentId, color));
    },
    move(id: number, targetParentId: number){
        const file = ft.delete(id);
        file.parentId = targetParentId;
        const folder = findFile(targetParentId) as MyFolder;
        folder.addFile(file);
    },
    delete(id: number){
        const file = findFile(id);
        if(!file) {
            throw new Error('file not found');
        }
        const parent = findFile(file.parentId) as MyFolder;
        parent.deleteFile(id);

        return file;
    },
    printFiles(dir: MyFolder) {
        let file = dir.files;

        while(file) {
            console.log(file.value.name);
            file = file.next;
        }
    },
    printAll
}
