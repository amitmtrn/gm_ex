
export abstract class OriginFile {
    constructor(public id: number, public name: string, public parentId: number) {}
}
