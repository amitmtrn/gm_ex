import { OriginFile } from "./OriginFile";

export class ListItem {
    public next: ListItem | null = null;
    constructor(public value: OriginFile) {
        
    }
}