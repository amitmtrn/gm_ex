import { OriginFile } from "./OriginFile";

export enum FileColors {
    Blue, White
}

export class MyFile extends OriginFile {
    constructor(id: number, name: string, parentId: number, public color: FileColors) {
        super(id, name, parentId);
    }
 }