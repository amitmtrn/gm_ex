import { ListItem } from "./ListItem";
import { OriginFile } from "./OriginFile";

export enum FolderColors {
    Red, Green, Blue
}


export class MyFolder extends OriginFile {
    files: ListItem| null = null;

    constructor(id: number, name: string, parentId: number, public color: FolderColors) {
        super(id, name, parentId);
    }

    findByName(fileName: string) {
        let file = this.files;
        while(file && file.value.name !== fileName) {
            file = file.next;
        }

        return file;

    }

    findFile(fileId: number) {
        let file = this.files;
        while(file && file.value.id !== fileId) {
            file = file.next;
        }

        return file;
    }

    addFile(file: OriginFile) {
        const item = new ListItem(file);
        item.next = this.files

        this.files = item;
    }

    deleteFile(fileId: number) {
        if(this.files === null) {
            throw new Error('no files in this folder');
        }

        if(this.files.next === null) {
            this.files = null;
            return;
        }

        let findFile: ListItem | null = this.files;
        let previousFile: ListItem = this.files;

        while(findFile && findFile.value.id !== fileId) {
            previousFile = findFile;
            findFile = findFile.next;
        }

        if(!findFile) {
            throw new Error('file/folder is not in this folder');
        }

        previousFile.next = findFile.next;
    }
 }
