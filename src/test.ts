import { ft } from './file_tree/index';
import { FileColors } from './file_tree/MyFile';
import { FolderColors } from './file_tree/MyFolder';

ft.addDir('test', 1, 0, FolderColors.Blue);
ft.addDir('test2', 2, 0, FolderColors.Blue);
ft.addDir('test3', 6, 1, FolderColors.Blue);
ft.addFile('file6', 7, 6, FileColors.White);
ft.addFile('somefile', 3, 1, FileColors.White);
ft.addFile('somefile2', 4, 1, FileColors.White);
ft.addFile('somefile3', 5, 1, FileColors.White);

ft.printAll(6);

// ft.move(7, 2);
// ft.printAll(6);
// ft.printAll(2);
// ft.delete(7);
// ft.printAll(2);
// ft.addFile('somefile8', 8, 6, FileColors.White);
// ft.addFile('somefile9', 9, 6, FileColors.White);
// ft.addFile('somefile10', 10, 6, FileColors.White);
// ft.printAll(6);